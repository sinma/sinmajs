# sinmajs

#### 介绍
sinma.js 常用的JS函数集合

#### 软件架构
软件架构说明
[说明书](https://www.kancloud.cn/sinma/sinmajs/3151960)






#### 安装教程

引用：
Javascript / Jquery版引用方法：
在Html文件中 html->body内任何地方：
<script src="script/sinma.js"></script>

Vuejs版引用方法：
<script src="script/sinma.js"></script>

#### 使用说明

使用：
Javascript / Jquery版使用方法：
请直接在代码中输入：
sinma.方法名
例如：
sinma.aaa('弹出测试');

Vuejs版使用方法：

请直接在代码中输入：
sinma.方法名
例如：
sinma.aaa('弹出测试');

[说明书](https://www.kancloud.cn/sinma/sinmajs/3151960)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. [www.sinma.net](http://www.sinma.net/)
3. QQ:42033223  欢迎交流
