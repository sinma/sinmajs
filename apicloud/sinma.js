//sinma.js v0.0.1  最后修改于2023-1-6
//因Apicloud已XX，故此文件不再更新和优化

(function(window) {
	var sinma = window.sinma || {};

	//判断是否数字
	sinma.isNumber = function(str) {
		var n = Number(str);
		if (!isNaN(n)) {
			return true;
		} else {
			return false;
		}
	}
	sinma.aaa = function(a){
		alert(a);
	}
	//判断是否对象
	sinma.isObj = function(obj) {
		if ( typeof obj == "object") {
			return true;
		} else {
			return false;
		}
	}
	//判断是否为空
	sinma.isEmpty = function(obj) {
		if (obj == "" || obj == null || obj == undefined || obj == "null" || obj == "undefined") {
			return true;
		} else {
			return false;
		}
	}
	//获取地址栏参数
	sinma.GetQueryString = function(name) {
		var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
		var r = window.location.search.substr(1).match(reg);
		if (r != null)
			return unescape(r[2]);
		return null;
	}

	sinma.GetQueryString2 = function(parm) {
		var reg = new RegExp("(^|&)" + parm + "=([^&]*)(&|$)", "i");
		var r = window.location.search.substr(1).match(reg);

		if (r != null)
			return unescape(r[2]);
		return null;
	}
	//需要注意的：unescape()函数（找到形式为 %xx 和 %uxxxx 的字符序列（x 表示十六进制的数字），用 Unicode 字符 \u00xx 和 \uxxxx 替换这样的字符序列进行解码，与之对应的是escape()编码），ECMAScript v3 已从标准中删除了 unescape() 函数，并反对使用它，因此应该用 decodeURI() 和 decodeURIComponent() 取而代之。注意：即便value 中有& 连接符，也没有关系，都可以通过encodeURIComponent进行转码成%26的形式，和浏览器自动拼接的并不冲突，记得decodeURIComponent()解码就好。

	//设置透明度 level (0-100)
	sinma.setOpacity = function(elem, level) {
		if (elem.filters) {//如果是IE
			elem.style.filter = 'alpha(opacity=' + level + ')';
			//必须设置zoom,要不然透明度在IE下不生效  From:http://blog.csdn.net/dxx1988/article/details/6581430
			elem.style.zoom = 1;
		} else {//否则是W3C
			elem.style.opacity = level / 100;
		}
	}
	//var img = "imgurl";//imgurl 就是你的图片路径

	sinma.getBase64Image = function(img) {
		var canvas = document.createElement("canvas");
		canvas.width = img.width;
		canvas.height = img.height;
		var ctx = canvas.getContext("2d");
		ctx.drawImage(img, 0, 0, img.width, img.height);
		var ext = img.src.substring(img.src.lastIndexOf(".") + 1).toLowerCase();
		var dataURL = canvas.toDataURL("image/" + ext);
		return dataURL;
	}
	//调用方法
	// var image = new Image();
	// image.src = img;
	// image.onload = function(){
	//   var base64 = getBase64Image(image);
	//   console.log(base64);
	// }

	//切割金额，超过万和亿则加万和亿的文字
	sinma.cutmoney = function(money) {
		if (money >= 10000000) {
			money = parseFloat(money / 10000000) + '亿';
		} else if (money < 10000000 && money >= 10000) {
			money = parseFloat(money / 10000) + '万';
		} else {
			money = parseFloat(money);
		}
		return money;
	}
	//判断用户名，如果没有真实姓名则用用户名
	sinma.getname = function(name, turename) {
		var username = name;
		if (turename && turename != '' && turename != 'null') {
			username = turename;
		}
		return username;
	}
	//判断用户头像，如果为空则用默认头像
	sinma.getuserpic = function(useravatar) {
		//alert(useravatar);
		var userpic = '../image/default_user_portrait.gif';
		if (useravatar && useravatar != '' && useravatar != 'null') {
			userpic = gip + 'data/upload/shop/avatar/' + useravatar;
		}
		return userpic;
	}
	//转换unix时间戳为年月日
	sinma.turntime = function(ut) {
		var dateObj = new Date(ut * 1000);
		var UnixTimeToDate = dateObj.getUTCFullYear() + ' 年 ' + (dateObj.getUTCMonth() + 1 ) + ' 月 ' + dateObj.getUTCDate() + ' 日 ';
		return UnixTimeToDate;
	}
	//转换时间格式字符串为时间
	sinma.turnDate = function(strDate) {
		var st = strDate;
		var a = st.split(" ");
		var b = a[0].split("-");
		var c = a[1].split(":");
		var date = new Date(b[0], b[1] - 1, b[2], c[0], c[1], c[2]);
		return date;
	}
	//转换unix时间戳为年月日小时分钟
	sinma.turntimemore = function(ut) {
		var dateObj = new Date(ut * 1000);
		var UnixTimeToDate = dateObj.getUTCFullYear() + ' 年 ' + (dateObj.getUTCMonth() + 1 ) + ' 月 ' + dateObj.getUTCDate() + ' 日  ' + dateObj.getUTCHours() + ':' + dateObj.getUTCMinutes();
		return UnixTimeToDate;
	}
	//去掉调用的null
	sinma.qunull = function(str) {
		if (!str || str == "null" || str == "Null" || typeof (str) == "undefined" || str == "undefined" || str == "NULL") {
			return '';
		} else {
			return str;
		}
	}
	sinma.testtel = function(tel){
		var telcheck = /^((1[3,4,5,6,7,8,9][0-9]))\d{8}$/;
		if(!telcheck.test(tel)){
			return false;
		}else{
			return true;
		}
	}
	sinma.testsfz = function(val) {
		var p = /^[1-9]\d{5}(18|19|20)\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/;
		p.test(val);
	}
	sinma.testsfz1 = function(val) {
		var checkProv = function(val) {
			var pattern = /^[1-9][0-9]/;
			var provs = {
				11 : "北京",
				12 : "天津",
				13 : "河北",
				14 : "山西",
				15 : "内蒙古",
				21 : "辽宁",
				22 : "吉林",
				23 : "黑龙江 ",
				31 : "上海",
				32 : "江苏",
				33 : "浙江",
				34 : "安徽",
				35 : "福建",
				36 : "江西",
				37 : "山东",
				41 : "河南",
				42 : "湖北 ",
				43 : "湖南",
				44 : "广东",
				45 : "广西",
				46 : "海南",
				50 : "重庆",
				51 : "四川",
				52 : "贵州",
				53 : "云南",
				54 : "西藏 ",
				61 : "陕西",
				62 : "甘肃",
				63 : "青海",
				64 : "宁夏",
				65 : "新疆",
				71 : "台湾",
				81 : "香港",
				82 : "澳门"
			};
			if (pattern.test(val)) {
				if (provs[val]) {
					return true;
				}
			}
			return false;
		}
		var checkDate = function(val) {
			var pattern = /^(18|19|20)\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)$/;
			if (pattern.test(val)) {
				var year = val.substring(0, 4);
				var month = val.substring(4, 6);
				var date = val.substring(6, 8);
				var date2 = new Date(year + "-" + month + "-" + date);
				if (date2 && date2.getMonth() == (parseInt(month) - 1)) {
					return true;
				}
			}
			return false;
		}
		var checkCode = function(val) {
			var p = /^[1-9]\d{5}(18|19|20)\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/;
			var factor = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2];
			var parity = [1, 0, 'X', 9, 8, 7, 6, 5, 4, 3, 2];
			var code = val.substring(17);
			if (p.test(val)) {
				var sum = 0;
				for (var i = 0; i < 17; i++) {
					sum += val[i] * factor[i];
				}
				if (parity[sum % 11] == code.toUpperCase()) {
					return true;
				}
			}
			return false;
		}
		if (checkCode(val)) {
			var date = val.substring(6, 14);
			if (checkDate(date)) {
				if (checkProv(val.substring(0, 2))) {
					return true;
				}
			}
		}
		return false;
	}
	//生成随机数字或字母
	var suijichars = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
	var suijinums = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
	sinma.suijishu = function(n, t) {
		var res = "";
		for (var i = 0; i < n; i++) {
			if (t == 1) {
				var id = Math.ceil(Math.random() * 35);
				res += suijichars[id];
			} else {
				var id = Math.ceil(Math.random() * 9);
				res += suijinums[id];
			}
		}
		return res;
	}
	//获取当前的日期时间 格式“yyyy-MM-dd HH:MM:SS”
	sinma.getnowDatetime = function() {
		var date = new Date();
		var seperator1 = "-";
		var seperator2 = ":";
		var month = date.getMonth() + 1;
		var strDate = date.getDate();
		if (month >= 1 && month <= 9) {
			month = "0" + month;
		}
		if (strDate >= 0 && strDate <= 9) {
			strDate = "0" + strDate;
		}
		var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate + " " + date.getHours() + seperator2 + date.getMinutes() + seperator2 + date.getSeconds();
		return currentdate;
	}
	//指定时间后的时间（特定格式）
	sinma.getnowDatetime1 = function(s) {
		var cdate = '';
		cdate = new Date(new Date().getTime() + s);
		var month = cdate.getMonth() + 1;
		var strDate = cdate.getDate();
		var strHour = cdate.getHours();
		var strMinu = cdate.getMinutes();
		var strSeco = cdate.getSeconds();
		if (month >= 1 && month <= 9) {
			month = "0" + month;
		}
		if (strDate >= 0 && strDate <= 9) {
			strDate = "0" + strDate;
		}
		if (strHour >= 0 && strHour <= 9) {
			strHour = "0" + strHour;
		}
		if (strMinu >= 0 && strMinu <= 9) {
			strMinu = "0" + strMinu;
		}
		if (strSeco >= 0 && strSeco <= 9) {
			strSeco = "0" + strSeco;
		}
		var currentdate = cdate.getFullYear() + month + strDate + strHour + strMinu + strSeco;
		//alert(currentdate);
		return currentdate;
	}
	//获取当前的日期 格式“yyyy-MM-dd”
	sinma.getnowDate = function() {
		var date = new Date();
		var seperator1 = "-";
		var month = date.getMonth() + 1;
		var strDate = date.getDate();
		if (month >= 1 && month <= 9) {
			month = "0" + month;
		}
		if (strDate >= 0 && strDate <= 9) {
			strDate = "0" + strDate;
		}
		var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate;
		return currentdate;
	}
	//获取指定的日期 格式“yyyy-MM-dd”
	sinma.getoldDate = function(ot) {
		var date = new Date(ot);
		var seperator1 = "-";
		var month = date.getMonth() + 1;
		var strDate = date.getDate();
		if (month >= 1 && month <= 9) {
			month = "0" + month;
		}
		if (strDate >= 0 && strDate <= 9) {
			strDate = "0" + strDate;
		}
		var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate;
		return currentdate;
	}
	//判断是否登录
	sinma.checklogin = function() {
		if ($api.getStorage("userphone") != '' && typeof ($api.getStorage("userphone")) != 'undefined' && $api.getStorage("userphone") != 'undefined') {
			return true;
		} else {
			api.toast({
				msg : '您尚未登录，现在为您返回登录页面',
				location : 'middle'
			});
			api.openWin({
				name : 'login',
				url : 'login.html',
				delay : 300
			});
			localStorage.setItem("userphone",'');
		}
	}
	//循环输出当前时间（到秒）
	sinma.tiaomiao = function(el) {
		//获得显示时间的div
		t_div = el;
		var now = new Date()
		//替换div内容
		t_div.innerHTML = "现在是" + now.getFullYear() + "年" + (now.getMonth() + 1) + "月" + now.getDate() + "日" + now.getHours() + "时" + now.getMinutes() + "分" + now.getSeconds() + "秒";
		//等待一秒钟后调用tiaomiao方法，由于settimeout在tiaomiao方法内，所以可以无限调用
		setTimeout(tiaomiao, 1000);
	}

	sinma.yzmdjs = function(el) {
		// 原生js版本
		//<input id="send" type="button" value="发送验证码">
		var times = 60, // 临时设为60秒
		timer = null;

		el.onclick = function() {
			// 计时开始
			timer = setInterval(function() {
				times--;
				if (times <= 0) {
					send.value = '发送验证码';
					clearInterval(timer);
					send.disabled = false;
					times = 60;
				} else {
					send.value = times + '秒后重试';
					send.disabled = true;
				}
			}, 1000);
		}
		// jQuery版本
		//		var times = 60, timer = null;
		//
		//		el.on('click', function() {
		//			var $this = $(this);
		//
		//			// 计时开始
		//			timer = setInterval(function() {
		//				times--;
		//
		//				if (times <= 0) {
		//					$this.val('发送验证码');
		//					clearInterval(timer);
		//					$this.attr('disabled', false);
		//					times = 60;
		//				} else {
		//					$this.val(times + '秒后重试');
		//					$this.attr('disabled', true);
		//				}
		//			}, 1000);
		//		});
	}

	sinma.checkRate = function(s) {
		var re = /^[0-9]+.?[0-9]*$/;
		if (!re.test(s)) {
			return false;
		} else {
			return true;
		}
	}
	sinma.openurl = function(urls) {
		api.confirm({
			title : 'APP版本更新-备用方案',
			msg : '系统检测到APP有新版本发布，立即更新？ ',
			buttons : ['确定', '取消']
		}, function(ret, err) {
			if (ret.buttonIndex == 1) {
				$api.clearStorage();
				api.openApp({
					androidPkg : 'Android.intent.action.VIEW',
					mimeType : 'text/html',
					uri : urls
				}, function(ret1, err1) {
					if (ret1) {
						//alert('ret:'+JSON.stringify(ret1));
					} else {
						//alert('err:'+JSON.stringify(err1));
						api.openWin({
							name : 'downurl',
							url : urls
						});
					}
				});
			}
		});
	}
	//图片按比例压缩
	/*sinma.setImgSize=function(){
	 var outbox_w=imgbox.width(),
	 outbox_h=imgbox.height();

	 imgbox.find('img').each(function(index, el) {
	 var obj=$(this),
	 objH=obj.height(),
	 objW=obj.width();

	 if((objH/objW)>(outbox_h/outbox_w)){
	 obj.css({
	 height: outbox_h+"px",
	 width: ((objW/objH)*outbox_h)+"px",
	 marginLeft:((outbox_w-((objW/objH)*outbox_h))/2)+"px"
	 　　　});
	 }else{
	 obj.css({
	 width: outbox_w+"px",
	 height: ((objH/objW)*outbox_w)+"px",
	 marginTop:((outbox_h-((objH/objW)*outbox_w))/2)+"px"
	 });
	 }
	 });
	 }*/
	window.sinma = sinma;
})(window);
