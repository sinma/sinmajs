//sinma.js v0.0.2  最后修改于2023-04-19

	//弹窗测试
	aaa = function(a){
		alert(a);
	}

	//判断是否数字
	isNumber = function(str) {
		let n = Number(str);
		return !isNaN(n);
	}
	//用正则判断是否数字
	isNumber2 = function(str) {
		let re = /^[0-9]+.?[0-9]*$/; //判断字符串是否为数字 （判断正整数 /^[1-9]+[0-9]*]*$/）
		return re.test(str);

	}

	//判断是否对象
	isObj = function(obj) {
		return typeof obj == "object";
	}
	//判断是否为空
	isEmpty = function(str) {
		return str == "" || str == null || str == undefined || str == "null" || str == "undefined";
	}

	//校验手机号(宽松)
	isPhonenum = function (str){
		let reg = /^(1[3-9]\\d{9}$)/;
		return reg.test(str);

	}
	//校验手机号(严格)
	isPhonenum1 = function (str){
		let reg = /^((13[0-9])|(14(0|[5-7]|9))|(15([0-3]|[5-9]))|(16(2|[5-7]))|(17[0-8])|(18[0-9])|(19([0-3]|[5-9])))\\d{8}$/;
		return reg.test(str);

	}
	//校验座机号(宽松)
	isTelnum = function (str){
		let reg = /^(\d3,4|\d{3,4}-)?\d{7,8}$/;
		return reg.test(str);
	}

	//校验座机号(严格)
	isTelnum1 = function (str){
		let reg = /^(([0-9]{3,4}-)?[0-9]{7,8}$)/;
		return reg.test(str);
	}

	//校验身份证号码(宽松)
	isSfz = function(val) {
		let p = /^[1-9]\d{5}(18|19|20)\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/;
		p.test(val);
	}
	//校验身份证号码(严格)
	isSfz1 = function(val) {
		let checkProv = function(val) {
			let pattern = /^[1-9][0-9]/;
			let provs = {
				11 : "北京",
				12 : "天津",
				13 : "河北",
				14 : "山西",
				15 : "内蒙古",
				21 : "辽宁",
				22 : "吉林",
				23 : "黑龙江 ",
				31 : "上海",
				32 : "江苏",
				33 : "浙江",
				34 : "安徽",
				35 : "福建",
				36 : "江西",
				37 : "山东",
				41 : "河南",
				42 : "湖北 ",
				43 : "湖南",
				44 : "广东",
				45 : "广西",
				46 : "海南",
				50 : "重庆",
				51 : "四川",
				52 : "贵州",
				53 : "云南",
				54 : "西藏 ",
				61 : "陕西",
				62 : "甘肃",
				63 : "青海",
				64 : "宁夏",
				65 : "新疆",
				71 : "台湾",
				81 : "香港",
				82 : "澳门"
			};
			if (pattern.test(val)) {
				if (provs[val]) {
					return true;
				}
			}
			return false;
		}
		let checkDate = function(val) {
			let pattern = /^(18|19|20)\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)$/;
			if (pattern.test(val)) {
				let year = val.substring(0, 4);
				let month = val.substring(4, 6);
				let date = val.substring(6, 8);
				let date2 = new Date(year + "-" + month + "-" + date);
				if (date2 && date2.getMonth() == (parseInt(month) - 1)) {
					return true;
				}
			}
			return false;
		}
		let checkCode = function(val) {
			let p = /^[1-9]\d{5}(18|19|20)\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/;
			let factor = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2];
			let parity = [1, 0, 'X', 9, 8, 7, 6, 5, 4, 3, 2];
			let code = val.substring(17);
			if (p.test(val)) {
				let sum = 0;
				for (let i = 0; i < 17; i++) {
					sum += val[i] * factor[i];
				}
				if (parity[sum % 11] == code.toUpperCase()) {
					return true;
				}
			}
			return false;
		}
		if (checkCode(val)) {
			let date = val.substring(6, 14);
			if (checkDate(date)) {
				if (checkProv(val.substring(0, 2))) {
					return true;
				}
			}
		}
		return false;
	}



	//获取地址栏参数
	GetQueryString = function(name) {
		let reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
		let r = window.location.search.substr(1).match(reg);
		if (r != null)
			return unescape(r[2]);
		return null;
	}

	GetQueryString2 = function(parm) {
		let reg = new RegExp("(^|&)" + parm + "=([^&]*)(&|$)", "i");
		let r = window.location.search.substr(1).match(reg);
		if (r != null)
			return unescape(r[2]);
		return null;
	}
	//需要注意的：unescape()函数（找到形式为 %xx 和 %uxxxx 的字符序列（x 表示十六进制的数字），用 Unicode 字符 \u00xx 和 \uxxxx 替换这样的字符序列进行解码，与之对应的是escape()编码），ECMAScript v3 已从标准中删除了 unescape() 函数，并反对使用它，因此应该用 decodeURI() 和 decodeURIComponent() 取而代之。注意：即便value 中有& 连接符，也没有关系，都可以通过encodeURIComponent进行转码成%26的形式，和浏览器自动拼接的并不冲突，记得decodeURIComponent()解码就好。

	//设置透明度 level (0-100)
	setOpacity = function(elem, level) {
		if (elem.filters) { //如果是IE
			elem.style.filter = 'alpha(opacity=' + level + ')';
			//必须设置zoom,要不然透明度在IE下不生效  From:http://blog.csdn.net/dxx1988/article/details/6581430
			elem.style.zoom = 1;
		} else { //否则是W3C
			elem.style.opacity = level / 100;
		}
	}

	//图片转化成base64
	//var img = "imgurl";//imgurl 就是你的图片路径
	getBase64Image = function(img) {
		let canvas = document.createElement("canvas");
		canvas.width = img.width;
		canvas.height = img.height;
		let ctx = canvas.getContext("2d");
		ctx.drawImage(img, 0, 0, img.width, img.height);
		let ext = img.src.substring(img.src.lastIndexOf(".") + 1).toLowerCase();
		return canvas.toDataURL("image/" + ext);
	}
	//调用方法
	// var image = new Image();
	// image.src = img;
	// image.onload = function(){
	//   var base64 = getBase64Image(image);
	//   console.log(base64);
	// }

	//切割金额，超过万和亿则加万和亿的文字
	cutmoney = function(money) {
		if (money >= 10000000) {
			money = parseFloat(money / 10000000) + '亿';
		} else if (money < 10000000 && money >= 10000) {
			money = parseFloat(money / 10000) + '万';
		} else {
			money = parseFloat(money);
		}
		return money;
	}
	//判断用户名，如果没有真实姓名则用用户名
	getname = function(name, turename) {
		let username = name;
		if (turename && turename != '' && turename != 'null') {
			username = turename;
		}
		return username;
	}
	//判断用户头像，如果为空则用默认头像
	getuserpic = function(useravatar) {
		//alert(useravatar);
		let userpic = '../image/default_user_portrait.gif';
		if (useravatar && useravatar != '' && useravatar != 'null') {
			userpic = gip + 'data/upload/shop/avatar/' + useravatar;
		}
		return userpic;
	}
	//转换unix时间戳为年月日
	turntime = function(ut) {
		let dateObj = new Date(ut * 1000);
		return dateObj.getUTCFullYear() + ' 年 ' + (dateObj.getUTCMonth() + 1) + ' 月 ' + dateObj.getUTCDate() + ' 日 ';
	}
	//转换时间格式字符串为时间
	turnDate = function(strDate) {
		let a = strDate.split(" ");
		let b = a[0].split("-");
		let c = a[1].split(":");
		return new Date(b[0], b[1] - 1, b[2], c[0], c[1], c[2]);
	}
	//转换unix时间戳为年月日小时分钟
	turntimemore = function(ut) {
		let dateObj = new Date(ut * 1000);
		return dateObj.getUTCFullYear() + ' 年 ' + (dateObj.getUTCMonth() + 1) + ' 月 ' + dateObj.getUTCDate() + ' 日  ' + dateObj.getUTCHours() + ':' + dateObj.getUTCMinutes();
	}
	//去掉调用的null
	qunull = function(str) {
		if (!str || str == "null" || str == "Null" || typeof (str) == "undefined" || str == "undefined" || str == "NULL") {
			return '';
		} else {
			return str;
		}
	}

	//生成随机数字或字母
	let suijichars = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
	let suijinums = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
	suijishu = function(n, t) {
		let res = "";
		for (let i = 0; i < n; i++) {
			if (t == 1) {
				let id = Math.ceil(Math.random() * 35);
				res += suijichars[id];
			} else {
				let id = Math.ceil(Math.random() * 9);
				res += suijinums[id];
			}
		}
		return res;
	}
	//获取当前的日期时间 格式“yyyy-MM-dd HH:MM:SS”
	getnowDatetime = function() {
		let date = new Date();
		let seperator1 = "-";
		let seperator2 = ":";
		let month = date.getMonth() + 1;
		let strDate = date.getDate();
		if (month >= 1 && month <= 9) {
			month = "0" + month;
		}
		if (strDate >= 0 && strDate <= 9) {
			strDate = "0" + strDate;
		}
		return date.getFullYear() + seperator1 + month + seperator1 + strDate + " " + date.getHours() + seperator2 + date.getMinutes() + seperator2 + date.getSeconds();
	}
	//指定时间后的时间（特定格式）
	getnowDatetime1 = function(s) {
		let cdate = new Date(new Date().getTime() + s);
		let month = cdate.getMonth() + 1;
		let strDate = cdate.getDate();
		let strHour = cdate.getHours();
		let strMinu = cdate.getMinutes();
		let strSeco = cdate.getSeconds();
		if (month >= 1 && month <= 9) {
			month = "0" + month;
		}
		if (strDate >= 0 && strDate <= 9) {
			strDate = "0" + strDate;
		}
		if (strHour >= 0 && strHour <= 9) {
			strHour = "0" + strHour;
		}
		if (strMinu >= 0 && strMinu <= 9) {
			strMinu = "0" + strMinu;
		}
		if (strSeco >= 0 && strSeco <= 9) {
			strSeco = "0" + strSeco;
		}
		return cdate.getFullYear() + month + strDate + strHour + strMinu + strSeco;
	}
	//获取当前的日期 格式“yyyy-MM-dd”
	getnowDate = function() {
		let date = new Date();
		let seperator1 = "-";
		let month = date.getMonth() + 1;
		let strDate = date.getDate();
		if (month >= 1 && month <= 9) {
			month = "0" + month;
		}
		if (strDate >= 0 && strDate <= 9) {
			strDate = "0" + strDate;
		}
		return date.getFullYear() + seperator1 + month + seperator1 + strDate;
	}
	//获取指定的日期 格式“yyyy-MM-dd”
	getoldDate = function(ot) {
		let date = new Date(ot);
		let seperator1 = "-";
		let month = date.getMonth() + 1;
		let strDate = date.getDate();
		if (month >= 1 && month <= 9) {
			month = "0" + month;
		}
		if (strDate >= 0 && strDate <= 9) {
			strDate = "0" + strDate;
		}
		return date.getFullYear() + seperator1 + month + seperator1 + strDate;
	}
	//判断是否登录
	checklogin = function() {
		if ($api.getStorage("userphone") != '' && typeof ($api.getStorage("userphone")) != 'undefined' && $api.getStorage("userphone") != 'undefined') {
			return true;
		} else {
			api.toast({
				msg : '您尚未登录，现在为您返回登录页面',
				location : 'middle'
			});
			api.openWin({
				name : 'login',
				url : 'login.html',
				delay : 300
			});
			$api.setStorage('userphone', '');
		}
	}
	//循环输出当前时间（到秒）
	tiaomiao = function(el) {
		//获得显示时间的div
		let t_div = el;
		let now = new Date()
		//替换div内容
		t_div.innerHTML = "现在是" + now.getFullYear() + "年" + (now.getMonth() + 1) + "月" + now.getDate() + "日" + now.getHours() + "时" + now.getMinutes() + "分" + now.getSeconds() + "秒";

		function time() {

		}

		//等待一秒钟后调用time方法，由于settimeout在time方法内，所以可以无限调用
		setTimeout(time, 1000);
	}


	 calculateAge = function(date1) {
	 	let birthday = new Date(date1); // 出生日期
		let currentDate = new Date(); // 当前日期
		let diff = Math.abs(currentDate - birthday); // 计算两个日期的时间戳差值
		return Math.floor(diff / (365 * 24 * 60 * 60 * 1000)); // 将差值转换为年份
	}
//验证码倒计时
	yzmdjs = function(el) {
		// 原生js版本
		//<input id="send" type="button" value="发送验证码">
		let times = 60, // 临时设为60秒
		timer = null;

		el.onclick = function() {
			// 计时开始
			timer = setInterval(function() {
				times--;
				let send;
				if (times <= 0) {
					send.value = '发送验证码';
					clearInterval(timer);
					send.disabled = false;
					times = 60;
				} else {
					send.value = times + '秒后重试';
					send.disabled = true;
				}
			}, 1000);
		}
	}


	//图片按比例压缩
	/*setImgSize=function(){
	 var outbox_w=imgbox.width(),
	 outbox_h=imgbox.height();

	 imgbox.find('img').each(function(index, el) {
	 var obj=$(this),
	 objH=obj.height(),
	 objW=obj.width();

	 if((objH/objW)>(outbox_h/outbox_w)){
	 obj.css({
	 height: outbox_h+"px",
	 width: ((objW/objH)*outbox_h)+"px",
	 marginLeft:((outbox_w-((objW/objH)*outbox_h))/2)+"px"
	 　　　});
	 }else{
	 obj.css({
	 width: outbox_w+"px",
	 height: ((objH/objW)*outbox_w)+"px",
	 marginTop:((outbox_h-((objH/objW)*outbox_w))/2)+"px"
	 });
	 }
	 });
	 }*/


module.exports = {
	aaa : aaa,
	isNumber : isNumber,
	isNumber2 : isNumber2,
	isObj : isObj,
	isEmpty : isEmpty,
	isPhonenum : isPhonenum,
	isPhonenum1 : isPhonenum1,
	isTelnum : isTelnum,
	isTelnum1 : isTelnum1,
	isSfz : isSfz,
	isSfz1 : isSfz1,
	GetQueryString : GetQueryString,
	GetQueryString2 : GetQueryString2,
	setOpacity : setOpacity,
	getBase64Image : getBase64Image,
	cutmoney : cutmoney,
	getname : getname,
	getuserpic : getuserpic,
	turntime : turntime,
	turnDate : turnDate,
	turntimemore : turntimemore,
	qunull : qunull,
	suijishu : suijishu,
	getnowDatetime:getnowDatetime,
	getnowDatetime1 : getnowDatetime1,
	getnowDate : getnowDate,
	getoldDate : getoldDate,
	checklogin : checklogin,
	tiaomiao : tiaomiao,
	calculateAge:calculateAge,
	yzmdjs : yzmdjs
}
